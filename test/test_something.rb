require "minitest/autorun"

class TestSomething < Minitest::Test
  def test_equality
    assert_equal 2, 2
  end

  def test_that_will_be_skipped
    skip "test this later"
  end
end